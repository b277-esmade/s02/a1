-- LIST DOWN/DISPLAY all the databases inside the DBMS

SHOW DATABASES;


--CREATE a database
-- Syntax: CREATE DATABASE <name_db>;

CREATE DATABASE music_db;



-- DELETE a database
-- Syntax: DROP DATABASE <name_db>

DROP DATABASE music_db;




-- SELECT a database
-- Syntax: USE <name_db>

USE music_db;



-- CREATE Tables
-- Table columns will also be declared here
-- Syntax: CREATE TABLE <table name>


CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number CHAR(11) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50),

-- To set primary key
	PRIMARY KEY(id)
);




-- DELETE a  table from database
-- DROP TABLE <table name>

DROP TABLE userss;


CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	playlist_name VARCHAR(30) NOT NULL,
	datetime_created DATETIME NOT NULL,

	PRIMARY KEY(id),
	-- Add rules to set foreign key and update synchronously when there are changes
	CONSTRAINT fk_playlists_user_id
				-- foreign key    --primary key from what table?
	FOREIGN KEY (user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);




-- DATE TIME
	-- created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	-- datetime_created DATETIME NOT NULL,


-- SHOW/DISPLAY table structure(columns, data type)
-- Syntax: DESCRIBE <name of the table>

DESCRIBE users;



-- INSERT data
-- Syntax: INSERT INTO <table name> (col1,col2 etc) VALUES (value1,value2);

INSERT INTO users(username,password,full_name,contact_number,email,address) VALUES ("evo", "evo","Eronna Mae Esmade","09123456789","evo@mail.com","MNL PH");





-- Artists Table

CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY(id)
);


-- Albums Table

CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	artist_id INT NOT NULL,
	album_title VARCHAR(50) NOT NULL,
	date_released DATETIME NOT NULL,
	PRIMARY KEY(id),

	CONSTRAINT fk_albums_artist_id
				-- foreign key    --primary key
	FOREIGN KEY (artist_id) REFERENCES artists(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);



-- Songs Table
CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	album_id INT NOT NULL,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(30),
	PRIMARY KEY(id),

	CONSTRAINT fk_songs_album_id
	FOREIGN KEY(album_id) REFERENCES albums(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);


-- Playlist_Songs Table
CREATE TABLE playlist_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY(id),

	CONSTRAINT fk_playlist_songs_playlist_id
	FOREIGN KEY(playlist_id) REFERENCES playlists(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,

	CONSTRAINT fk_playlist_songs_song_id
	FOREIGN KEY(song_id) REFERENCES songs(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);